# Overview
Lightweight-ish Linux (Ubuntu with XFCE) Vagrant-managed VM and forkable project template. Multi-purpose, config-as-code, command-line and GUI, and automation-oriented.

For individuals or teams.

Heavily inspired by [Manual Work Is A Bug](https://queue.acm.org/detail.cfm?id=3197520) or ABA (always be automating). That essay gives language and clarity to a bunch of super-important, practical engineering concepts. Deep gratitude to [Parsia](https://parsiya.net/) for sharing it, and to [Thomas Limoncelli](https://twitter.com/yesthattom) for writing it.

# Quickstart
1. Clone/fork to a host with Vagrant + Virtualbox
2. `cd repo-dir && vagrant up` (the VM uses 3GB RAM by default)

Put notes/CLI snippets/screenshots/etc. in Joplin within the VM, and they'll be stored in the repo's `Joplin` directory for easy version-control.

# Default Credentials
* `vagrant/vagrant`: non-root, passwordless sudoer user. Vagrant/Ansible depends on this user (so be careful with `$HOME/.ssh/authorized_keys`).

# Feature Highlights
* Joplin for documentation (markdown-based, saves to the project's `/Joplin` by default)
* Flameshot for quick-capture/edit/emphasize screenshots
* Guake + zsh + Oh My Zsh accessible via F12/F11 (pulldown terminal)

# ABA Philosophy
Seriously, you should read [Manual Work Is A Bug](https://queue.acm.org/detail.cfm?id=3197520).

ABA (always be automating) is exactly right, in these ways (among others):
* An ABA culture and libre software is how we build on each other's work, pushing things forward.
* If an ABA culture and work style can be put into practice (by individuals or engineering teams), it's a massive superpower.

# Tech Stack Summary
* Default tooling/config oriented towards FOSS, security, privacy, productivity, and automation
* Vagrant to manage the VM's lifecycle
* Ansible to manage software & config within the VM (idempotent by default)
* Ubuntu for the OS and package repos

# Suggested Uses
* Disposable Linux VM with swappable packages (via tweaking Ansible role)
* Forkable project template (docs created using Joplin in-VM are automatically added to project's `Joplin` directory)
* Growing an ABA discipline/culture within a team
* Learning/exploration platform (personal use)

# Project Goals
* Lightweight-ish by default (low number of running processes, curated autostart)
* Discoverable GUI and command-line
* Enable & support automation projects

# Recommended Hardware & RAM Config
This VM uses 3GB of RAM by default (configurable via `Vagrantfile` ->  `v.memory`).

Your physical host should have at least 4GB of RAM (ideally 8GB+). CPUs in most desktops/laptops from say, 2015 on should be fine.

During development, post-boot desktop environment (XFCE before launching heavy apps like browser/IDE), RAM usage was in the 500-600MB range. On systems with 4GB of RAM, you might be able to get away with a `v.memory = 2048`.

Depending on your use case/package selection, the VM can benefit from more RAM, especially if you're using heavyweight software like a proper IDE or browser in-VM. If you're doing this, carve out as much RAM from your host system as practical (6GB or more).

# Setup/Usage Guide
## Prereq: install host software
Start with a reasonably modern desktop Linux, Windows, or MacOS bare-metal machine. Then install:
* `vagrant`
* `virtualbox`
* `git`
* A terminal emulator (Cmder or WSL are good on Windows, iTerm on MacOS, whatever you want on Linux under X/Wayland/whatnot)
Joplin
To manage Vagrant and Virtualbox installs, it's usually best to use your OS package manager (like `chocolatey` on Windows, your Linux distro's package manager, or `brew` on MacOS). Vagrant and Virtualbox should both be on recent, compatible versions (check Vagrant release notes).

## Step 1: Choose a usage pattern (disposable VM/fork)
### Disposable VM
For disposable VM usage just clone, `vagrant up`, and go.

### Fork/Project Template
Fork this repo somewhere and start using the VM for your project. Save your screenshots/CLI snippets/notes/docs using Joplin in the VM (autosaves to the project's `/Joplin` directory), or elsewhere in this repo's directory structure. Commit changes to the forked repo.

This repository is designed as an on-ramp to an ABA SDL. You can use it as a platform for open-collaboration, ABA-style "wiggling" between CLI snippets, writing docs/taking screenshots, code, with code next to docs under version control, etc.

To fully benefit from this ABA platform, customize this repo to align with you (or your team's) habits/environment/use cases/tooling. For example, add a role that imports your shell config, installs your tooling, convenience scripts for your project, etc. Habit-form around the built-in software+config, or adapt for your own use.

## Step 2: Use the VM
1. `cd repo_dir`
2. `vagrant up`
3. Either `vagrant ssh` in if you just need CLI, or use the GUI otherwise
4. When you're trying to save or share your work, commit changes back to the forked repo

## Common VM Commands
* `vagrant reload --provision` (to force a reboot + re-apply VM's config), `vagrant up --provision` (avoids a reboot)
* `vagrant destroy` periodically, so you're never too tied to a single VM's saved state, fork this repo as needed instead
* `vagrant box update` followed by `vagrant destroy -f && vagrant up` to update the upstream OS (this wipes the VM, commit/save whatever you need first)

## VM Usage Tips
* To open a pulldown terminal emulator press F12, F11 will fullscreen it. `Ctrl+Shift+T` opens a tab, `Ctrl+Shift+W` closes current tab. You may also want to run `~/dotfile-setup.sh` for a fancy Guake+zsh+OhMyZsh setup.
* Explore the Ansible roles, desktop shortcuts, the menu, the as much of my own setup builds on top of it. filesystem (especially `$HOME` and dotfiles) to start grokking how it works, so you can build on this VM yourself.

# Gotchas
* If you're using Windows as your Vagrant+Virtualbox host, you might have to play with git CRLF settings before running `git clone` (https://stackoverflow.com/questions/4181870/git-on-windows-what-do-the-crlf-settings-mean)
* Performance tends to suck if you don't have VT-x CPU extensions. Check your VT-x setup if you hit hard-to-pinpoint, weird performance issues.
* No stability/support guarantees; that said, I try to keep this repo's `master` reasonably stable, as it's an important part of my computing setup.

# External Links
- https://queue.acm.org/detail.cfm?id=3197520
- https://jroberts.io

## TODOs
[TODOs.md](TODOs.md)
