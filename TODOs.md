# TODOs
* Set up Firefox default profile with privacy-oriented extensions
* Set up Firefox pentest profile with pentest-oriented extensions
* Alter Firefox desktop shortcut launch with `-p` to prompt for profile
* Add default+customizable wallpaper
* Add screenshots
* Add checksumming to Joplin installation (and version-pinning in other places as appropriate)
* Illustrate how to extend ABA platform (within Joplin)
* Add a pomodoro timer accessible directly on desktop (widget) and via CLI
* Add Conky + a beautiful+functional default config+theme
* Add pre-built Docker shortcut to host a static web server of `/vagrant/staticsite`
* Automatic periodic restic backup of $HOME to /vagrant/backups
* Implement branch structure (lightweight, full, master, proprietary-extra, messenger)
* Squash commits before publishing (retain commit history privately for learning)
* Add Pidgin+OTR
* Add Signal
* Inherit full package list from base-generalpurpose-gui VM
* Add more keybinds
* Add ABA intro slidedeck
* Add Joplin CLI
* Refactors user Ansible vars + docs to incorporate habit-building
* Improve dotfile Ansible setup experience in fancy-guake-zsh-cli
* Add HOWTO docs for fancy-guake-zsh-cli
* Add draw.io shortcut
* Implement skeleton Ansible roles
