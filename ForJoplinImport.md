# Use-as-Platform Summary
do some/all of:
1. `cd` to the repo directory, run `vagrant up`, and use the VM as-is for easy access to docker, docker-compose, Kali top 10 tools metapackage, Firefox, Tor browser bundle, OnionShare, and a whole bunch of other security/privacy/productivity/oriented tools. If you want to save your work easily, use `vagrant snapshot`, the Joplin app installed in the VM (which saves its files to `/Joplin` in this repo for easy saving/collaboration).
2. Play around with and version-control changes to `roles/` to sculpt this VM into what you want. Like installing programs, setting up config files (or jinja2-style templating them), cloning git repos, anything from the suite of automation enabled by the combo of git+Vagrant+Virtualbox+Ansible+Docker+DockerCompose by altering the Ansible base role directly or adding your own Ansible roles (via the proper folder structure under `/roles` and tweaking `playbook.yml` accordingly). Try to keep changes idempotent by using the built-in Ansible modules or writing your programs to run the same way every time invoked with whatever arguments, and make small, logically-related, useful-commit-message type commits (big iterative commits are OK too, sometimes you just have to write a bunch of stuff at once!).
3. Use this repo as an automation "launchpad" for longer-term projects inspired by ABA. Like an iterative base. It's meant to be a "launchpad" that makes it easy to "wiggle" between ABA phases 1-3.

# Easy Tweaks
* Swap in `kalilinux/rolling` Vagrant box via the `Vagrantfile` whenever you need access to full Kali toolset
* Configure RAM in the `Vagrantfile`. `lightweight` branches have 4GB by default (for a comfortable process load without a full-blown IDE), or `master` and `full`-style branches default to 6GB (in case you want to edit the project through a heavyweight IDE in the VM directly, or otherwise run a heavy process load).
* Disable the GUI in the `Vagrantfile` if you intend to use this VM only via `vagrant ssh`
* Add your own Kali metapackages (https://tools.kali.org/kali-metapackages) or other packages available through `apt` to `vars/kali_vars.yml` (or just editing `roles/base/tasks/main.yml`).
* Most Debian-oriented Ansible roles can be easily added to `roles` (with corresponding changes in `playbook.yml`).
* (as superuser/admin on host) `vagrant plugin install vagrant-disksize`

# Design Goals/Aspirations
* Lightweight (lowish-process-count, minimal and opinionated autostart + desktop icons)
* Preloaded with productivity+collaboration+security+privacy+FOSS oriented tools
* Usable as non-root `vagrant` user (https://www.kali.org/news/kali-default-non-root-user/)
* Easy to get started with as long as you've got a reasonably beefy (8GB of RAM and a moderately-powerful CPU should do) base host has git, Vagrant, and Virtualbox, and some reasonable CPU/RAM (8GB is comfortable, the default desktop environment is under 1GB of RAM usage before launching Firefox)
* Discoverable, lightweight, and extensible via Ansible (Debian-derived distro Ansible roles should work with not-too-much tweaking)
* GUI-and-CLI friendly in equal parts
* The default CLI launched via panel icon is a preloaded default-bash CLI

As-cloned/forked, this repo is idempotent and redeployable via `vagrant destroy -f && vagrant up`. That way, if things go wrong, and you're comfortable starting over from the base version-controlled config, run the command above. Note that this throws away changes made via the UI and not somehow recorded/screenshotted/explicitly captured in version control), Kali VM with top 10 tools and other package installs/config tweaks for convenience and productivity.

# Screenshots
TODO
## Desktop
### Default

### Menu

### htop

# Tooling
## VM Infra Stack
- git
- vagrant
- virtualbox
- ansible
