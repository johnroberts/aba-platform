# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  ##### BASE BOX #####
  # config.vm.box = "debian/buster64"
  # config.vm.box = "bento/debian-10"
  config.vm.box = "ubuntu/bionic64"

  ##### NETWORKING #####
  config.vm.network "private_network", ip: "192.168.22.11"
  config.vm.hostname = "aba-platform"

  ##### GUEST ADDITION PLUGIN #####
  #config.vbguest.allow_downgrade = true

  ##### VIRTUALBOX CUSTOMIZATION ######
  config.vm.provider "virtualbox" do |v|
    v.gui = true
    v.memory = 3072

    # Bidirectional clipboard
    # v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
  end

  ##### WINDOWS WORKAROUNDS ######
  # Shared folders from NTFS filesystems are seen in-VM as world-writeable 777 (https://github.com/ansible/ansible/issues/42388)
  # Solution from https://github.com/ansible/ansible/issues/42388#issuecomment-405950940
  config.vm.synced_folder ".", "/vagrant",  mount_options: ["dmode=775"]

  ##### PROVISIONING VIA ANSIBLE ######
  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "playbook.yml"
    ansible.install = true
    ansible.install_mode = :default
  end

  ##### POST-PROVISIONING REBOOT ######
  config.vm.provision "shell", inline: "reboot"
end
